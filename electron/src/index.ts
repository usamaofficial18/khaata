import { app } from "electron";
import {
  createCapacitorElectronApp,
  createCapacitorElectronDeepLinking,
} from "@capacitor-community/electron";

export const customProtocol = 'acc-app';

// The MainWindow object can be accessed via myCapacitorApp.getMainWindow()
const capacitorApp = createCapacitorElectronApp({
  splashScreen: { useSplashScreen: false },
  mainWindow: {
    windowOptions : {
      autoHideMenuBar: true,
      webPreferences: { nodeIntegration: true },
    },
  },
});

createCapacitorElectronDeepLinking(capacitorApp, { customProtocol });

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some Electron APIs can only be used after this event occurs.
app.on("ready", () => {
  capacitorApp.init();
});

// Quit when all windows are closed.
app.on("window-all-closed", function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (capacitorApp.getMainWindow().isDestroyed()) capacitorApp.init();
});

// Login pop-over
app.on('browser-window-created', (event, win) => {
  if (win.getTitle() === '_self') {
    win.webContents.addListener('did-redirect-navigation', (event, url) => {
      if (url.includes(`${customProtocol}://`)) {
        const slug = url.split(`${customProtocol}://`).pop();
        capacitorApp.getMainWindow().loadURL(`capacitor-electron://-/${slug}`)
        win.close();
      }
    });
  }
});
// Define any IPC or other custom functionality below here
