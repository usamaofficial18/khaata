export const environment = {
  production: true,

  // Configure Variables
  appURL: 'http://cap.localhost:4200',
  authorizationURL:
    'http://test.localhost:8000/api/method/frappe.integrations.oauth2.authorize',
  authServerURL: 'http://test.localhost:8000',
  callbackProtocol: 'frappe',
  callbackUrl: 'capacitor-electron://-/callback',
  clientId: '8bdc49394d',
  profileURL:
    'http://test.localhost:8000/api/method/frappe.integrations.oauth2.openid_profile',
  revocationURL:
    'http://test.localhost:8000/api/method/frappe.integrations.oauth2.revoke_token',
  scope: 'openid all',
  tokenURL:
    'http://test.localhost:8000/api/method/frappe.integrations.oauth2.get_token',
  isAuthRequiredToRevoke: false,
};

// client_id is available for public, client_secret is to be protected.
