import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MaterialModule } from '../../material/material.module';
import {
  DeleteDialog,
  DialogOverviewExampleDialog,
} from './update-transaction-dialog';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaterialModule,
    ReactiveFormsModule,
  ],
  declarations: [DialogOverviewExampleDialog, DeleteDialog],
})
export class RecordDialogModule {}
