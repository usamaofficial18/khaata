import { TestBed } from '@angular/core/testing';
import {
  dataStoreFactory,
  DATA_STORE_TOKEN,
} from '../../../auth/data-store/data-store.factory';

import { AccountService } from './account.service';

describe('AccountService', () => {
  let service: AccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: DATA_STORE_TOKEN, useFactory: dataStoreFactory }],
    });
    service = TestBed.inject(AccountService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
