import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { IonicModule } from '@ionic/angular';
import {
  dataStoreFactory,
  DATA_STORE_TOKEN,
} from '../auth/data-store/data-store.factory';
import { AccountService } from '../ledger/entities/account/account.service';

import { DashboardPage } from './dashboard.page';

describe('DashboardPage', () => {
  let component: DashboardPage;
  let fixture: ComponentFixture<DashboardPage>;
  let service: AccountService;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [DashboardPage],
        imports: [IonicModule.forRoot()],

        providers: [
          { provide: DATA_STORE_TOKEN, useFactory: dataStoreFactory },
          {
            provide: Router,
            useValue: {},
          },
          {
            provide: MatDialog,
            useValue: {},
          },
        ],
      }).compileComponents();
      service = TestBed.inject(AccountService);
      fixture = TestBed.createComponent(DashboardPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
