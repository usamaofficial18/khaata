import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DashboardPageRoutingModule } from './dashboard-routing.module';
import { MaterialModule } from '../material/material.module';
import { DashboardPage } from './dashboard.page';
import { RecordDialogModule } from '../common/record-dialog/record-dialog.module';
import { TransactionTableComponent } from '../transaction-table/transaction-table.component';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaterialModule,
    ReactiveFormsModule,
    DashboardPageRoutingModule,
    RecordDialogModule,
    FlexLayoutModule,
  ],
  declarations: [DashboardPage, TransactionTableComponent],
})
export class DashboardPageModule {}
