import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppComponent } from './app.component';
import { AddNameDialog, HomePage } from './home/home.page';
import { AppRoutingModule } from './app-routing.module';
import { TokenService } from './auth/token/token.service';
import { StorageService } from './auth/storage/storage.service';
import { HttpClientModule } from '@angular/common/http';
import {
  dataStoreFactory,
  DATA_STORE_TOKEN,
} from './auth/data-store/data-store.factory';
import { AccountService } from './ledger/entities/account/account.service';
import { MaterialModule } from './material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TransactionTableComponent } from './transaction-table/transaction-table.component';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { DashboardPage } from './dashboard/dashboard.page';
import { DialogOverviewExampleDialog } from './common/record-dialog/update-transaction-dialog';
import { HomePageModule } from './home/home.module';
import { TransactionTableModule } from './transaction-table/transaction-table.module';
import { DashboardPageModule } from './dashboard/dashboard.module';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    HttpClientModule,
    HomePageModule,
    DashboardPageModule,
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    TokenService,
    StorageService,
    { provide: DATA_STORE_TOKEN, useFactory: dataStoreFactory },
    AccountService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
