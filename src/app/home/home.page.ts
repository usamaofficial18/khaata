import { Component, Inject, OnInit } from '@angular/core';
import { switchMap } from 'rxjs/operators';
import { AccountService } from '../ledger/entities/account/account.service';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogData, STATUS } from '../common/interface';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { from, of } from 'rxjs';
import { DialogOverviewExampleDialog } from '../common/record-dialog/update-transaction-dialog';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  constructor(public dialog: MatDialog) {}

  ngOnInit() {}

  openDialog() {
    const dialogRef = this.dialog.open(AddNameDialog, {
      width: '60vh',
      data: {},
    });
    dialogRef.afterClosed().subscribe(result => {});
  }

  openRecordDialog() {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '45vh',
      data: {},
    });
    dialogRef.afterClosed().subscribe(result => {});
  }
}

@Component({
  selector: 'add-name-dialog',
  templateUrl: 'add-name-dialog.html',
})
export class AddNameDialog {
  transactionStatus: string[] = [STATUS.CREDIT, STATUS.DEBIT];
  transactionHistoryForm = new FormGroup({
    status: new FormControl('', []),
    name: new FormControl('', []),
  });

  constructor(
    public dialogRef: MatDialogRef<AddNameDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private accountService: AccountService,
    private snackbar: MatSnackBar,
  ) {
    if (this.data) {
      Object.keys(this.transactionHistoryForm.controls).forEach(element => {
        this.transactionHistoryForm.get(element).setValue(data[element]);
      });
    }
  }

  updateEntry(type) {
    switch (type) {
      case 'update':
        this.updateTransaction().subscribe();
        break;

      case 'noclick':
        this.onNoClick();
        break;

      default:
        this.snackbar.open('Please select the correct option', 'Close', {
          duration: 3000,
        });
        break;
    }
  }

  updateTransaction() {
    const data = {} as DialogData;
    return from(
      this.accountService.findOne({
        status: 'Name',
        name: this.transactionHistoryForm.controls.name.value,
      }),
    ).pipe(
      switchMap(data => {
        if (data) {
          this.snackbar.open('Name Already Exists', 'Close', {
            duration: 3000,
          });
          return of(true);
        }
        this.dialogRef.close();
        return from(
          this.accountService.save({
            status: 'Name',
            name: this.transactionHistoryForm.controls.name.value,
          }),
        );
      }),
    );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
